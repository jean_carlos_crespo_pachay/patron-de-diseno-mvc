﻿using MVC_PARA_EXPONER.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_PARA_EXPONER.Controllers
{
    public class MienbrosController : Controller
    {
        // GET: Mienbros
        public ActionResult Index()
        {
            //return View();
            var mienbros = from a in exponermienbros()
                           orderby a.ID
                           select a;
            return View(mienbros);
        }

        // GET: Mienbros/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Mienbros/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Mienbros/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Mienbros/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Mienbros/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Mienbros/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Mienbros/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [NonAction]
        public List<Mienbro> exponermienbros()
        {
            return new List<Mienbro>
            {
                new Mienbro
                {
                    ID=1,
                    nombre="JEAN",
                    Apodo="FLACO",
                    Apellido="Crespo",
                    fecha_de_incorporacion=DateTime.Parse(DateTime.Today.ToString()),
                    edad=19
                },
                 new Mienbro
                {
                    ID=2,
                    nombre="juan",
                    Apodo="Horse",
                    Apellido="Mora",
                    fecha_de_incorporacion=DateTime.Parse(DateTime.Today.ToString()),
                    edad=25
                },
                  new Mienbro
                {
                    ID=3,
                    nombre="ariel",
                    Apodo="bronce",
                    Apellido="segovia",
                    fecha_de_incorporacion=DateTime.Parse(DateTime.Today.ToString()),
                    edad=3
                },
                   new Mienbro
                {
                    ID=4,
                    nombre="manuel",
                    Apodo="big",
                    Apellido="perez",
                    fecha_de_incorporacion=DateTime.Parse(DateTime.Today.ToString()),
                    edad=15
                } };


            }
        }
    }

